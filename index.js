var express = require('express');
var request = require('request');
var mp3Duration = require('mp3-duration');
var $ = require('jquery');
var fs = require('fs');

var app = express();

app.set('port', (process.env.PORT || 8888));

app.use(express.static(__dirname + '/public'));
app.use('/music', express.static(__dirname + '/music'));

// views is directory for all template files
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

app.get('/', function(request, response) {
	response.render('pages/index');
});

app.get('/getmusic', function(request, response) {
	response.send(songDetails);
});

app.listen(app.get('port'), function() {
	console.log('Node app is running on port', app.get('port'));
});

var songDetails = null;

// Collect song data before page loads
var spawn = require('child_process').spawn;
pyprog = spawn('python', [__dirname + '/public/python/songData.py', __dirname + "/music/"]);

pyprog.stdout.on('data', function(data) {
	songDetails = JSON.parse(data);
	console.log('Song data loaded successfully');
});

pyprog.stderr.on('data', function(data) {
	console.log("Failure: " + data.toString());
});

function getFolder(path) {
	return fs.readdirSync(path);
}