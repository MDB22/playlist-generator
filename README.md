#### Test folder:
- Files and functionality for basic webapp
- Files and libraries for Bootstrap theme

#### Node functions:
node app.js

 - Launches the Playlist Webapp

node gulpfile.js

 - Instantiates the gulp automation tasks used to build the Bootstrap theme docs

#### Files:
package.json - defines the project "meta-data"