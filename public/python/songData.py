from mutagen.mp3 import MP3
import json
import sys
import os

songs = {}

for root, dirs, files in os.walk(sys.argv[1]):

	path = root.split("/")

	style = path[-1]

	if not style:
		continue

	songs[style] = [];

	for file in files:
		song = {'title': file}

		song['duration'] = MP3(root + "/" + file).info.length

		songs[style] += [song]

print(json.dumps(songs))