var playlist_uri = undefined;
var newPlaylistID = undefined;
var nameSet = false;

var currentPlaylist = 'No playlist selected/created'

/**
* Retrieves user's playlist when button is clicked
*/

/**
* Obtains parameters from the hash of the URL
* @return Object
*/
function getHashParams() {
	var hashParams = {};
	var e, r = /([^&;=]+)=?([^&;]*)/g,
	    q = window.location.hash.substring(1);
	while ( e = r.exec(q)) {
	   hashParams[e[1]] = decodeURIComponent(e[2]);
	}
	return hashParams;
}

function createPlaylist(name) {

	var params = getHashParams();

	var access_token = params.access_token;

	var url = 'https://api.spotify.com/v1/users/' + user_id + '/playlists';

  $.ajax(url, {
    method: 'POST',
    data: JSON.stringify({
      'name': name,
      'public': false
    }),
    dataType: 'json',
    headers: {
      'Authorization': 'Bearer ' + access_token,
      'Content-Type': 'application/json'
    },
    success: function(r) {
      playlist_uri = r.uri;
      newPlaylistID = r.id;
      console.log(r);
      setPlaylist(name);
    },
    error: function(r) {
      console.log("Failure.")
    }
  });
}

function setPlaylist(name) {
	if (!nameSet) {

    currentPlaylist = 'Songs in "' + name + '"';

		$('#playlist-title h4').html(currentPlaylist);

		$('.flextable').hide();

		nameSet = true;

    updateTrackList();
	}
}

function updateTrackList() {

  var params = getHashParams();

  var access_token = params.access_token;

  $.ajax({
    url: 'https://api.spotify.com/v1/users/' + user_id + '/playlists/' + newPlaylistID + '/tracks',
    headers: {
      'Authorization': 'Bearer ' + access_token
    },
    success: function(response) {

      var html = '<li id="playlist-title" class="list-group-header"><h4>' + currentPlaylist + '</h4></li>'

      var tracks = response.items;

      tracks.forEach(function(item, index) {
        html += "<li class='list-group-item list-group-item-action justify-content-between'>";
        html += "<div class='col-md-6 song-name'>" + 
              item.track.name +
            "</div>";
        html += "<div class='col-md-6 text-right'>" +
              "<button class='btn btn-outline-success delete'>Delete</button>" + 
            "</div>"
        html += "</li>";
      });

      $('#song-list').html(html);
      
      $('.delete').click(deleteSong);

    }
  });
}

function submitName(name, textboxInput) {

	var params = getHashParams();

  	var access_token = params.access_token;

  	// If the name was sent from the textBox,
  	// make sure the playlist doesn't already exist
  	if (textboxInput) {
  		$.ajax({
          url: 'https://api.spotify.com/v1/me/playlists',
          headers: {
            'Authorization': 'Bearer ' + access_token
          },
          success: function(response) {

          	var playlists = response.items;

          	var exists = false;

          	playlists.forEach(function(item, index) {
          		if (item.name === name) {
          			exists = true;
          			alert("Playlist already exists; try a different name, or select the playlist on the left");
          		} 
          		return;
          	});

          	// Create new playlist
          	if (!exists) {
          		createPlaylist(name);
            }
          }
      	});
  	}
  	// Otherwise, the user selected this name intentionally from the list,
  	// which is completely fine, but we still need the playlist URI
  	else {
  		$.ajax({
          url: 'https://api.spotify.com/v1/me/playlists',
          headers: {
            'Authorization': 'Bearer ' + access_token
          },
          success: function(response) {

          	var playlists = response.items;

          	playlists.forEach(function(item, index) {
          		if (item.name === name) {
          			playlist_uri = item.uri
                newPlaylistID = item.id;
          		} 
          		return;
          	});

      		  setPlaylist(name);

          }
      	});
  	}
};

(function() {

	$('#create-playlist').click(function() {
		submitName($('#playlist-entry').val(), true);
	});

})();

(function() {

	$("input").keypress(function(event) {
		if (event.which == 13) {
			submitName($('#playlist-entry').val(), true);
		}
	});

})();