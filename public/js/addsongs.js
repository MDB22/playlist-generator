function addSong(genre, playlistID) {

	if (!nameSet) {
		alert("Select a playlist before adding songs.")
	} else {

	  var params = getHashParams();

	  var access_token = params.access_token;

	  // Retrieve tracks from the playlist we're adding a song from
	  $.ajax({
	    url: 'https://api.spotify.com/v1/users/' + user_id + '/playlists/' + playlistID + '/tracks',
	    headers: {
	      'Authorization': 'Bearer ' + access_token
	    },
	    success: function(response) {

	  	  // Randomly select a song URI to add to the new playlist
	  	  if (response.items.length <= 0) {
	  	  	return;
	  	  }
	  	  
	      var item = response.items[Math.floor(Math.random() * response.items.length)];

	      var url = 'https://api.spotify.com/v1/users/' + user_id + '/playlists/' + newPlaylistID + '/tracks'

		  $.ajax(url, {
		    method: 'POST',
		    data: JSON.stringify({
		      'uris': [item.track.uri]
		    }),
		    dataType: 'json',
		    headers: {
		      'Authorization': 'Bearer ' + access_token,
		      'Content-Type': 'application/json'
		    },
		    success: function(r) {
		      console.log(item.track.name + " added to playlist from " + genre)
		      updateTrackList();
		    },
		    error: function(r) {
		      console.log("Failure.")
		    }
		  });

	    }
	  });
	}

}