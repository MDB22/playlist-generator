var nameSet = false;

var slider = $('#songProgress').slider({});

slider.on('change', function(event) {
	var time = event.value.newValue;
	updateCurrentTime(time, true);
});

var audio = document.getElementById("audioPlayer");

audio.onended = function() {
	// TODO: Progress to next track in playlist
}

audio.ontimeupdate = function () {
	updateCurrentTime(audio.currentTime, false);
}

function playAudio(path, songDetails) {

	stopAudio();

	audio.src = path + songDetails['title'];

    toggleMusic();

    slider = $('#songProgress').slider({
    	max: songDetails['duration']
    });

    $("#currentSong").html(songDetails['title']);
    $('#duration').html(secondsToMinutes(songDetails['duration']));
    $("#songDetails").show();
} 

function stopAudio() {
	audio.pause();
}

function secondsToMinutes(duration) {
	var minutes = Math.floor(duration/60);
	var seconds = Math.floor(duration - minutes * 60);
	if (seconds < 10) {
		seconds = "0" + seconds;
	}
	return minutes + ":" + seconds;
}

function updateCurrentTime(time, scrubMusic) {
	$('#currentTime').html(secondsToMinutes(time));
	slider.slider('setValue', time, false, false);

	if (scrubMusic) {
		audio.currentTime = time;
	}
}

function setPlaylistName(name) {
	if (!nameSet) {

    	playlistName = 'Songs in <i>' + name + '</i>';

		$('#title').html(playlistName);

		$('.flextable').hide();

		nameSet = true;
	}
}

$(document).ready(function() {

	$("#songDetails").hide();

		$.ajax({
	        url: "/getmusic",
	        method: "GET"
	    })
	    .done(function(folderList) {
	    	addListsToPage(folderList);
	    })
	    .fail(function() {
	    	console.log("fail");
	    });
	});

(function() {

	$('#create-playlist').click(function() {
		setPlaylistName($('#playlist-entry').val());
	});

	// Reponse to Enter keypress
	$("input").keypress(function(event) {
		if (event.which == 13) {
			setPlaylistName($('#playlist-entry').val());
		}
	});

	$('#rewind').click(function() {
		updateCurrentTime(0, true);
		// TODO: restart current song, or go back one song if song ended
	});
	$('#play').click(function() {
		toggleMusic();
	});
	$('#stop').click(function() {
		audio.pause();
		$('#play').html('<span class="icon icon-controller-play"></span>');
	});
	$('#skip').click(function() {
		// TODO
	});
})();

function toggleMusic() {
	if (audio.paused) {
		audio.play();
		$('#play').html('<span class="icon icon-controller-paus"></span>');
	} else {
		audio.pause();
		$('#play').html('<span class="icon icon-controller-play"></span>');
	}
}

function addListsToPage(folderList) {

	songList = folderList;

  	var html = $('#style-list').html();

  	for (var key in folderList) {
  		html += "<li class='list-group-item justify-content-between'>";
  		html += "<div class='col-md-4 style-name' id=" + key + "><h5><i>" + 
  					key +
  				"</i></h5></div>";
  		html += "<div class='col-md-8 text-right'>" + 
  					"<button class='btn btn-outline-success add'>Add Song</button></span>" + 
  					"<span>&nbsp;</span>" + 
  					"<button class='btn btn-outline-success remove'>Remove Style</button></span>" +
  					"<span>&nbsp;</span>" + 
  					"<button class='btn btn-outline-success expand' data-toggle='collapse' data-target='#" + key + "songs'>Expand</button></span>" 
  				"</div>";
  		html += "</li>";
 	}

// <div id="accordion" role="tablist">
//   <div class="card">
//     <div class="card-header" role="tab" id="headingOne">
//       <h5 class="mb-0">
//         <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
//           Collapsible Group Item #1
//         </a>
//       </h5>
//     </div>

//     <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
//       <div class="card-body">
//         Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
//       </div>
//     </div>
//   </div>
//   <div class="card">
//     <div class="card-header" role="tab" id="headingTwo">
//       <h5 class="mb-0">
//         <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
//           Collapsible Group Item #2
//         </a>
//       </h5>
//     </div>
//     <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
//       <div class="card-body">
//         Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
//       </div>
//     </div>
//   </div>
//   <div class="card">
//     <div class="card-header" role="tab" id="headingThree">
//       <h5 class="mb-0">
//         <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
//           Collapsible Group Item #3
//         </a>
//       </h5>
//     </div>
//     <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
//       <div class="card-body">
//         Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
//       </div>
//     </div>
//   </div>
// </div>

  	$('#style-list').html(html);

  	$("#get-music").hide();

  	// $('.playlist-name').click(function() {
  	// 	var playlist_name = $(this).html();
  	// 	submitName(playlist_name, false);
  	// });

  	// $('.add').click(function() {
  		
   //  	var playlist_name = playlist_obj.html();
   //  	addSong(playlist_name, playlistIds[playlist_name]);
  	// });

  	$('.add').click(function() {		
  		var style = $(this).parent().siblings('.style-name').attr('id');

  		var index = Math.floor(Math.random() * songList[style].length);

	    playAudio('music/' + style + '/', songList[style][index]);
  	});

  	$('.remove').click(function() {
  		var parent = $(this).closest('li').remove();
  	});
}